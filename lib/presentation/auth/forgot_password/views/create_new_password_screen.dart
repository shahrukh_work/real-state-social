import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../constants/app_assets/app_images.dart';
import '../../../../global/app_theme/app_colors.dart';
import '../../../../global/app_widgets/app_bar_back_button.dart';
import '../../../../global/app_widgets/app_button.dart';
import '../../../../global/app_widgets/app_text_form_field.dart';
import '../../../../global/app_widgets/app_text_widget.dart';
import '../../../../main.dart';

class CreateNewPasswordScreen extends StatelessWidget {
   CreateNewPasswordScreen({super.key});
  final passwordTextController = TextEditingController();
  final confirmPasswordTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Scaffold(
      body:
      SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: mq.height*0.05),
              AppBarBackButton(),
              SizedBox(height: mq.height*0.05),
              SvgPicture.asset(AppImages.createNewPassScreenImage),
              SizedBox(height: mq.height*0.015),
              AppTextWidget(text: 'Create new password', color: AppColors.black, fontSize: 24,fontWeight: FontWeight.w500,),
              SizedBox(height: mq.height*0.015),
              RichText(textAlign:TextAlign.center,text: TextSpan(children: [
                TextSpan(text: 'Please enter and confirm your new password. ',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400)),
                TextSpan(text: '\nYou will need to login after you reset.',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400,)),
        
              ])),
              SizedBox(height: mq.height*0.05,),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: AppTextWidget(text: 'Password', color: AppColors.black, fontSize: 15,fontWeight: FontWeight.w500,)),
              ),
        
              SizedBox(height: mq.height*0.01,),
              AppTextFormField(controller: passwordTextController, inputType: TextInputType.emailAddress, hintText: 'Enter password',),
              SizedBox(height: mq.height*0.05),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: AppTextWidget(text: 'Confirm Password', color: AppColors.black, fontSize: 15,fontWeight: FontWeight.w500,)),
              ),

              SizedBox(height: mq.height*0.01,),
              AppTextFormField(controller: passwordTextController, inputType: TextInputType.emailAddress, hintText: 'Enter confirm password',),
              SizedBox(height: mq.height*0.01,),
              RichText(textAlign: TextAlign.center,text: TextSpan(children: [
                TextSpan(text: 'Didn\'t received the code? ',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400)),
                TextSpan(text: 'Resend code',style: TextStyle(color: AppColors.primaryColor,fontSize: 14,fontWeight: FontWeight.w700)),
                TextSpan(text: '\n\n   Resend code in 59:00 ',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400,)),
        
              ])),
              SizedBox(height: mq.height*0.1,),
              AppButton(titleText: 'Reset password', onPressed: (){
                // Get.toNamed(createNewPasswordScreen);
              }, textColor: AppColors.white),
        
            ],
          ),
        ),
      ),
    );
  }
}
