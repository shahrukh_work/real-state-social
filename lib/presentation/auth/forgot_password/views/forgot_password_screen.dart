import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:pool_real_estate/config/routes/app_routes.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';
import 'package:pool_real_estate/global/app_widgets/app_bar_back_button.dart';
import 'package:pool_real_estate/global/app_widgets/app_button.dart';
import 'package:pool_real_estate/global/app_widgets/app_text_widget.dart';

import '../../../../global/app_theme/app_colors.dart';
import '../../../../global/app_widgets/app_text_form_field.dart';
import '../../../../main.dart';

class ForgotPasswordScreen extends StatelessWidget {
   ForgotPasswordScreen({super.key});
  final emailTextController= TextEditingController();

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Scaffold(
      body:
      SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: mq.height*0.05),
              AppBarBackButton(),
              SizedBox(height: mq.height*0.05),
              SvgPicture.asset(AppImages.forgotPasswordScreenImage),
              SizedBox(height: mq.height*0.05,),
              AppTextWidget(text: 'Forgot password', color: AppColors.black, fontSize: 24,fontWeight: FontWeight.w500,),
              SizedBox(height: mq.height*0.01,),
              AppTextWidget(text: 'No worries! Enter your email address below and we will send you a code to reset password.', color: AppColors.black, fontSize: 14,fontWeight: FontWeight.w400,textAlign: TextAlign.center,),
              SizedBox(height: mq.height*0.05,),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: AppTextWidget(text: 'Email', color: AppColors.black, fontSize: 15,fontWeight: FontWeight.w500,)),
              ),
              SizedBox(height: mq.height*0.01,),
              AppTextFormField(controller: emailTextController, inputType: TextInputType.emailAddress, hintText: 'Enter your verified email',),
              SizedBox(height: mq.height*0.2,),
              AppButton(titleText: 'Send code', onPressed: ()=>Get.toNamed(verifyAccountScreen), textColor: AppColors.white),
        
            ],
          ),
        ),
      ),
    );
  }
}
