import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:pool_real_estate/presentation/auth/signup/controller/signup_controller.dart';
import 'package:pool_real_estate/presentation/walk_through/widgets/divider_widget.dart';

import '../../../../constants/app_assets/app_icons.dart';
import '../../../../global/app_theme/app_colors.dart';
import '../../../../global/app_widgets/app_button.dart';
import '../../../../global/app_widgets/app_text_form_field.dart';
import '../../../../global/app_widgets/app_text_widget.dart';
import '../../../../main.dart';

class SignupScreen extends StatelessWidget {
  SignupScreen({super.key});
  final emailTextController = TextEditingController();
  final firstNameTextController = TextEditingController();
  final lastNameTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  final confirmPasswordTextController = TextEditingController();
  final signupController = Get.put(SignupController());

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Scaffold(
      backgroundColor: AppColors.appBgColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: mq.height * 0.1,
            ),
            Center(
                child: AppTextWidget(
              text: 'Sign Up',
              color: AppColors.black,
              fontSize: 18,
              fontWeight: FontWeight.w600,
            )),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTextWidget(
                    text: 'First name',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppTextFormField(
                    controller: firstNameTextController,
                    inputType: TextInputType.emailAddress,
                    hintText: 'Enter first name',
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8, bottom: 10),
                      child: SvgPicture.asset(
                        AppIcons.userNameIcon,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppTextWidget(
                    text: 'Last name',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  AppTextFormField(
                    controller: lastNameTextController,
                    inputType: TextInputType.emailAddress,
                    hintText: 'Enter last name',
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8, bottom: 10),
                      child: SvgPicture.asset(
                        AppIcons.userNameIcon,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppTextWidget(
                    text: 'Email address',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  AppTextFormField(
                    controller: emailTextController,
                    inputType: TextInputType.emailAddress,
                    hintText: 'Enter email',
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8, bottom: 10),
                      child: SvgPicture.asset(
                        AppIcons.userEmailIcon,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppTextWidget(
                    text: 'Password',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  AppTextFormField(
                    controller: passwordTextController,
                    inputType: TextInputType.visiblePassword,
                    hintText: 'Enter password',
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                        right: 8,
                        bottom: 10,
                      ),
                      child: SvgPicture.asset(
                        AppIcons.userPasswordIcon,
                      ),
                    ),
                    suffixIcon: Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                        right: 8,
                        bottom: 10,
                      ),
                      child: SvgPicture.asset(
                        AppIcons.passwordHiddenIcon,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppTextWidget(
                    text: 'Confirm Password',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  AppTextFormField(
                    controller: confirmPasswordTextController,
                    inputType: TextInputType.visiblePassword,
                    hintText: 'Enter password',
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                        right: 8,
                        bottom: 10,
                      ),
                      child: SvgPicture.asset(
                        AppIcons.userPasswordIcon,
                      ),
                    ),
                    suffixIcon: Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                        right: 8,
                        bottom: 10,
                      ),
                      child: SvgPicture.asset(
                        AppIcons.passwordHiddenIcon,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppTextWidget(
                    text: 'Select role',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  Obx(
                    () => DropdownButton<String>(
                      dropdownColor: AppColors.appBgColor,
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      style: TextStyle(color: AppColors.black.withOpacity(0.5)),
                      value: signupController.selectedRole.value,
                      hint: Text('Select role'),
                      icon: SvgPicture.asset(
                          AppIcons.dropDownIcon), // dropdown icon
                      isExpanded: true, // expand dropdown to cover full width
                      underline: DividerWidget(
                        dividerColor: AppColors.appGrey,
                      ), // remove underline
                      onChanged: (String? newValue) {
                        if (newValue != null) {
                          signupController.selectedRole.value = newValue;
                        }
                      },
                      items: <String>['Mortgage broker', 'User', 'Agent']
                          .map<DropdownMenuItem<String>>(
                        (String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: AppTextWidget(
                              text: value,
                              color: AppColors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.w200,
                            ),
                          );
                        },
                      ).toList(),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppButton(
                      titleText: 'Sign Up',
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                      onPressed: () {},
                      buttonColor: AppColors.primaryColor,
                      textColor: AppColors.white),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: 'Already have an account? ',
                          style: TextStyle(
                              color: AppColors.appGrey,
                              decoration: TextDecoration.underline,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      TextSpan(
                          text: 'Sign In',
                          style: TextStyle(
                              color: AppColors.appGrey,
                              decoration: TextDecoration.underline,
                              fontSize: 14,
                              fontWeight: FontWeight.w700)),
                    ])),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
