import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:pool_real_estate/config/routes/app_routes.dart';

import '../../../../constants/app_assets/app_images.dart';
import '../../../../global/app_theme/app_colors.dart';
import '../../../../global/app_widgets/app_bar_back_button.dart';
import '../../../../global/app_widgets/app_button.dart';
import '../../../../global/app_widgets/app_text_form_field.dart';
import '../../../../global/app_widgets/app_text_widget.dart';
import '../../../../main.dart';

class VerifyAccountscreen extends StatelessWidget {
   VerifyAccountscreen({super.key});
  final otpTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Scaffold(
      body:
      SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: mq.height*0.05),
              AppBarBackButton(),
              SizedBox(height: mq.height*0.05),
              SvgPicture.asset(AppImages.verifyAccountScreenImage),
              SizedBox(height: mq.height*0.01),
              AppTextWidget(text: 'Verify account', color: AppColors.black, fontSize: 24,fontWeight: FontWeight.w500,),
              SizedBox(height: mq.height*0.01),
              RichText(text: TextSpan(children: [
                TextSpan(text: 'Code has been sent to ',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400)),
                TextSpan(text: 'johndoe@gmail.com.',style: TextStyle(color: AppColors.primaryColor,fontSize: 14,fontWeight: FontWeight.w700)),
                TextSpan(text: '\n   Enter the code to verify your account.',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400,)),
        
              ])),
              SizedBox(height: mq.height*0.05,),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: AppTextWidget(text: 'Enter code', color: AppColors.black, fontSize: 15,fontWeight: FontWeight.w500,)),
              ),
        
              SizedBox(height: mq.height*0.01,),
              AppTextFormField(controller: otpTextController, inputType: TextInputType.emailAddress, hintText: 'Enter code',),
              SizedBox(height: mq.height*0.05),
              RichText(textAlign: TextAlign.center,text: TextSpan(children: [
                TextSpan(text: 'Didn\'t received the code? ',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400)),
                TextSpan(text: 'Resend code',style: TextStyle(color: AppColors.primaryColor,fontSize: 14,fontWeight: FontWeight.w700)),
                TextSpan(text: '\n\n   Resend code in 59:00 ',style: TextStyle(color: AppColors.black,fontSize: 14,fontWeight: FontWeight.w400,)),
        
              ])),
              SizedBox(height: mq.height*0.1,),
              AppButton(titleText: 'Verify account', onPressed: ()=>Get.toNamed(createNewPasswordScreen), textColor: AppColors.white),
        
            ],
          ),
        ),
      ),
    );
  }
}
