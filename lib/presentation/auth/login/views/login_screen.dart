import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:pool_real_estate/config/routes/app_routes.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';
import 'package:pool_real_estate/global/app_widgets/app_button.dart';
import 'package:pool_real_estate/global/app_widgets/app_text_form_field.dart';
import 'package:pool_real_estate/global/app_widgets/app_text_widget.dart';

import '../../../../constants/app_assets/app_icons.dart';
import '../../../../global/app_theme/app_colors.dart';
import '../../../../main.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Scaffold(
      backgroundColor: AppColors.appBgColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: mq.height * 0.1,
            ),
            Center(child: SvgPicture.asset(AppImages.loginScreenImage)),
            SizedBox(
              height: mq.height * 0.01,
            ),
            Center(
                child: AppTextWidget(
              text: 'Sign In',
              color: AppColors.black,
              fontSize: 18,
              fontWeight: FontWeight.w600,
            )),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTextWidget(
                    text: 'Email',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  AppTextFormField(
                    controller: emailTextController,
                    inputType: TextInputType.emailAddress,
                    hintText: 'Enter email',
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8, bottom: 10),
                      child: SvgPicture.asset(
                        AppIcons.userEmailIcon,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  AppTextWidget(
                    text: 'Password',
                    color: AppColors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: mq.height * 0.01,
                  ),
                  AppTextFormField(
                    controller: passwordTextController,
                    inputType: TextInputType.emailAddress,
                    hintText: 'Enter password',
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                        right: 8,
                        bottom: 10,
                      ),
                      child: SvgPicture.asset(
                        AppIcons.userPasswordIcon,
                      ),
                    ),
                    suffixIcon: Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                        right: 8,
                        bottom: 10,
                      ),
                      child: SvgPicture.asset(
                        AppIcons.passwordHiddenIcon,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  InkWell(
                    onTap: () => Get.toNamed(forgotPasswordScreen),
                    child: Align(
                        alignment: Alignment.topRight,
                        child: AppTextWidget(
                          text: 'Forgot Password?',
                          color: Color(0xFF417899),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          textDecoration: TextDecoration.underline,
                        )),
                  ),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  AppButton(
                      titleText: 'Sign In',
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                      onPressed: () => Get.toNamed(mainScreen),
                      buttonColor: AppColors.primaryColor,
                      textColor: AppColors.white),
                  SizedBox(
                    height: mq.height * 0.02,
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed(signupScreen);
                    },
                    child: Align(
                      alignment: Alignment.center,
                      child: RichText(
                          text: TextSpan(children: [
                        TextSpan(
                            text: 'Don\'t have an account? ',
                            style: TextStyle(
                                color: AppColors.appGrey,
                                decoration: TextDecoration.underline,
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        TextSpan(
                            text: 'Sign Up',
                            style: TextStyle(
                                color: AppColors.appGrey,
                                decoration: TextDecoration.underline,
                                fontSize: 14,
                                fontWeight: FontWeight.w700)),
                      ])),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
