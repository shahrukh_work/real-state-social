import 'package:flutter/material.dart';

import '../../../global/app_theme/app_colors.dart';
import '../../../main.dart';

class DividerWidget extends StatelessWidget {
  final Color? dividerColor;
  const DividerWidget({super.key, this.dividerColor});

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Container(height: 1,width: mq.width*1,color: dividerColor ?? AppColors.black);
  }
}
