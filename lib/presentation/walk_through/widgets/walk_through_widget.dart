import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pool_real_estate/constants/app_assets/app_icons.dart';
import 'package:pool_real_estate/global/app_widgets/app_text_widget.dart';

import '../../../global/app_theme/app_colors.dart';
import '../../../main.dart';
import 'divider_widget.dart';

class WalkThroughWidget extends StatelessWidget {
  final String textOne;
  final String textTwo;
  final bool isTextVisible;
  final VoidCallback onTap;

  const WalkThroughWidget({super.key, required this.textOne, required this.textTwo, required this.isTextVisible, required this.onTap});

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Container(

      // margin: EdgeInsets.all(8),
      child: Card(

        elevation: 5,
        child: Container(
          padding: EdgeInsets.all(20),
          decoration:BoxDecoration(
            color: AppColors.white,
              borderRadius:BorderRadius.circular(10),
            border: Border.all(color: AppColors.black,width: 0.5)
          ) ,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppTextWidget(text: textOne, color: AppColors.black, fontSize: 40,fontWeight: FontWeight.w600,textAlign: TextAlign.left,),
              SizedBox(height: mq.height*0.01,),
              AppTextWidget(text: textTwo, color: AppColors.black, fontSize: 17,fontWeight: FontWeight.w400,textAlign: TextAlign.left,),
              SizedBox(height: mq.height*0.027,),
              DividerWidget(),
              SizedBox(height: mq.height*0.027,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AppTextWidget(text:isTextVisible== false ? '' : 'GET STARTED', color: AppColors.black, fontSize: 20,fontWeight: FontWeight.w600,),
                  InkWell(
                      onTap: onTap,
                      child: SvgPicture.asset(AppIcons.arrowForwardIcon)),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
