import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:pool_real_estate/config/routes/app_routes.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';
import 'package:pool_real_estate/global/app_widgets/app_text_widget.dart';
import 'package:pool_real_estate/presentation/walk_through/widgets/walk_through_widget.dart';

import '../../../global/app_theme/app_colors.dart';
import '../../../main.dart';
import 'package:flutter_svg/flutter_svg.dart';


class WalkThroughScreenOne extends StatelessWidget {
  const WalkThroughScreenOne({super.key});

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Scaffold(
      backgroundColor: AppColors.appBgColor,
      body:
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: mq.height*0.01,),
            AppTextWidget(text: 'Logo', color: AppColors.black, fontSize: 28,fontWeight: FontWeight.w600,),
            SizedBox(height: mq.height*0.05,),
            SvgPicture.asset(AppImages.walkThroughImageOne),

            SizedBox(height: mq.height*0.2,),
            WalkThroughWidget(textOne: 'Pool\nReal Estate', textTwo: 'This is an example of how to use an animated gradient in the interface.', isTextVisible: false, onTap: (){
              Get.toNamed(walkThroughScreenTwo);
            },),

          ],
        ),
      ),
    );
  }
}
