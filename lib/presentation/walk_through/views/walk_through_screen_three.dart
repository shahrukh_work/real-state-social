import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../config/routes/app_routes.dart';
import '../../../constants/app_assets/app_images.dart';
import '../../../global/app_theme/app_colors.dart';
import '../../../global/app_widgets/app_text_widget.dart';
import '../../../main.dart';
import '../widgets/walk_through_widget.dart';

class WalkThroughScreenThree extends StatelessWidget {
  const WalkThroughScreenThree({super.key});

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Scaffold(
      backgroundColor: AppColors.appBgColor,
      body:
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: mq.height*0.01,),
            AppTextWidget(text: 'Logo', color: AppColors.black, fontSize: 28,fontWeight: FontWeight.w600,),
            SizedBox(height: mq.height*0.05,),
            SvgPicture.asset(AppImages.walkThroughImageThree),
            SizedBox(height: mq.height*0.13,),
            WalkThroughWidget(textOne: 'Pool\nReal Estate', textTwo: 'This is an example of how to use an animated gradient in the interface.', isTextVisible: true, onTap: (){
              Get.toNamed(loginScreen);
            },),

          ],
        ),
      ),
    );
  }
}
