import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:pool_real_estate/config/routes/app_routes.dart';
import 'package:pool_real_estate/global/app_theme/app_colors.dart';
import 'package:pool_real_estate/global/app_widgets/app_text_widget.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // Call the function to navigate after 2 seconds
    Timer(Duration(seconds: 2), navigateToNextScreen);
  }

  void navigateToNextScreen() {
    // Navigate to the next screen using named routes
    Get.offNamed(walkThroughScreenOne);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Center(child: AppTextWidget(text: 'Logo', color: AppColors.black, fontSize: 28,fontWeight: FontWeight.w600,),),
    );
  }
}
