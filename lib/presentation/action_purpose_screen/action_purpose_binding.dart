
import 'package:get/get.dart';
import 'action_purpose_controller.dart';

class ActionPurposeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(()=> ActionPurposeController());
  }
}