
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';


class ActionPurposeController extends GetxController
{

  var selectedItems = List<String>.empty(growable: true).obs;
  var selectPartner = List<String>.empty(growable: true).obs;
  var selectBuyerPaymentGrowable = List<String>.empty(growable: true).obs;

  final List<String> selectedPurposeList = [
    'Buy and Hold',
    'Rent Out',
    'Renovate and Sell',
  ];
  final List<String> paymentTypeList = [
    'I am a cash buyer',
    'I don’t have full amount available.',
  ].obs;
  final List<String> wantPartnerList = [
    'Yes please find me a match',
  ];

  void toggleItem(String item) {
    if (selectedItems.contains(item)) {
      selectedItems.remove(item);
    } else {
      selectedItems.add(item);
    }
  }
  void togglePartnerSerch(String item) {
    if (selectPartner.contains(item)) {
      selectPartner.remove(item);
    } else {
      selectPartner.add(item);
    }
  }
  void togglebuyerPayment(String item) {
    if (selectBuyerPaymentGrowable.contains(item)) {
      selectBuyerPaymentGrowable.remove(item);

    } else {
      selectBuyerPaymentGrowable.add(item);
    }
  }

  void checkIndexClick(bool checkindex){


  }




  var currentIndex = 0.obs;

  void goToPage(int index) {
    currentIndex.value = index;
  }
  // var currentStep = 0.obs;
  // var isCompleted = false.obs;
  //
  // void incrementStep() {
  //   final isLastStep = currentStep.value == getSteps().length - 1;
  //   if (isLastStep) {
  //     isCompleted.value = true;
  //     print("Completed");
  //   } else {
  //     currentStep.value += 1;
  //   }
  // }
  //
  // void decrementStep() {
  //   if (currentStep.value > 0) {
  //     currentStep.value -= 1;
  //   }
  // }
  //
  // List<Step> getSteps() => [
  //   Step(
  //
  //      label: SvgPicture.asset("assets/images/ic_firs_step_image.svg"),
  //     state: currentStep.value > 0 ? StepState.complete : StepState.indexed,
  //     isActive: currentStep.value >= 0,
  //     content: Column(
  //       children: [
  //         Text("shujja"),
  //       ],
  //     ),
  //     title: Text("Account"),
  //   ),
  //   Step(
  //     state: currentStep.value > 1 ? StepState.complete : StepState.indexed,
  //     isActive: currentStep.value >= 1,
  //     title: Text("Address"),
  //     content: Column(
  //       children: [
  //         Text("shahrukh"),
  //       ],
  //     ),
  //   ),
  //   Step(
  //     state: currentStep.value > 2 ? StepState.complete : StepState.indexed,
  //     isActive: currentStep.value >= 2,
  //     title: Text("Complete"),
  //     content: Column(
  //       children: [
  //         Text("jamaml"),
  //       ],
  //     ),
  //   ),
  // ];

  var selectedPaymentValue = "".obs;

  void setSelectedPaymentValue(String value) {
    selectedPaymentValue.value = value;
  }
}