import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pool_real_estate/constants/app_assets/app_font.dart';
import 'package:pool_real_estate/constants/app_assets/app_icons.dart';
import 'package:pool_real_estate/global/app_theme/app_colors.dart';

import '../../global/app_widgets/app_button.dart';
import 'action_purpose_controller.dart';

class ActionPurposeScreen extends StatelessWidget {
  final ActionPurposeController actionPurposeController =
      Get.find<ActionPurposeController>();

  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.of(context).size;
    // final RxString selectedFruit = "".obs;
    final List<String> fruitList = [
      "Apple",
      "Banana",
      "Orange",
      "Grapes",
      "Mango"
    ];
    // var option;
    var option1;
    return SafeArea(
      child: Scaffold(
          backgroundColor: AppColors.appBgColor,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              InkWell(
                onTap: () => Get.back(),
                child: Image.asset("assets/images/ic_arrow_back.png"),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: [
                    StepperComponent(
                      imagePath: "assets/images/stepper_image_one.svg",
                      index: 0,
                      onTap: () => actionPurposeController.goToPage(0),
                      subTittle: "Purpose",
                    ),
                    Obx(
                      () => StepperComponent(
                        imagePath:
                            actionPurposeController.currentIndex.value >= 1
                                ? "assets/images/stepper_image_part_two.svg"
                                : "assets/images/stepper_image_part_one.svg",
                        index: 1,
                        onTap: () => actionPurposeController.goToPage(1),
                        subTittle: "Amount",
                      ),
                    ),
                    Obx(
                      () => StepperComponent(
                        imagePath: actionPurposeController.currentIndex.value >=
                                2
                            ? "assets/images/stepper_image_three_part_two.svg"
                            : "assets/images/stepper_image_three_part_one.svg",
                        index: 2,
                        isLast: true,
                        onTap: () => actionPurposeController.goToPage(2),
                        subTittle: "Buyer",
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: Colors.black.withOpacity(.5),
              ),
              Expanded(
                child: PageView.builder(
                  itemCount: 3,
                  physics: NeverScrollableScrollPhysics(),
                  padEnds: true,
                  pageSnapping: false,
                  allowImplicitScrolling: true,
                  controller: PageController(),
                  onPageChanged: (index) =>
                      actionPurposeController.goToPage(index),
                  itemBuilder: (context, index) {
                    return Obx(() => actionPurposeController
                                .currentIndex.value ==
                            0
                        ? Padding(
                            padding: const EdgeInsets.only(left: 15, right: 24),
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.only(left: 15, right: 24),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    // color: Colors.green,
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 16,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 15),
                                        child: Text(
                                          "Select Purpose",
                                          style: TextStyle(
                                              fontFamily: AppFont.poppinsFont,
                                              color: Color(
                                                  0xff1D3A74), // Text color when selected,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Divider(),
                                      Container(
                                        // color: Colors.red,
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: actionPurposeController
                                              .selectedPurposeList.length,
                                          itemBuilder: (context, index) {
                                            final option =
                                                actionPurposeController
                                                    .selectedPurposeList[index];
                                            return Obx(() => Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    CheckboxListTile(
                                                      title: Text(
                                                        option,
                                                        style: TextStyle(
                                                            fontFamily: AppFont
                                                                .poppinsFont,
                                                            color: actionPurposeController
                                                                    .selectedItems
                                                                    .contains(
                                                                        option)
                                                                ? Color(
                                                                    0xff1D3A74)
                                                                : Colors
                                                                    .black, // Text color when selected
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize: 12),
                                                      ),
                                                      value:
                                                          actionPurposeController
                                                              .selectedItems
                                                              .contains(option),
                                                      onChanged: (bool? value) {
                                                        if (value != null) {
                                                          actionPurposeController
                                                              .toggleItem(
                                                                  option);
                                                        }
                                                      },
                                                      controlAffinity:
                                                          ListTileControlAffinity
                                                              .trailing,
                                                      checkColor:
                                                          Color(0xff1D3A74),
                                                      selectedTileColor: Colors
                                                          .transparent, // Ensure the selected tile color is transparent
                                                      activeColor: Colors
                                                          .white, // Adjust the color as needed
                                                      checkboxShape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                5), // Adjust border radius as needed
                                                        side: BorderSide(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      side: BorderSide(
                                                          color: Colors.black),
                                                      dense: true,
                                                    ),
                                                    Divider(),
                                                  ],
                                                ));
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        : actionPurposeController.currentIndex.value == 1
                            ? Padding(
                                padding: EdgeInsets.only(left: 15, right: 24),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    // color: Colors.green,
                                  ),
                                  child: SingleChildScrollView(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 16,
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(left: 15),
                                          child: Text(
                                            "Property Price",
                                            style: TextStyle(
                                                fontFamily: AppFont.poppinsFont,
                                                color: Color(
                                                    0xff1D3A74), // Text color when selected,
                                                fontWeight: FontWeight.w400,
                                                fontSize: 15),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    left: 15,
                                                    top: 11.5,
                                                    bottom: 11.5),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                    color: Colors.white),
                                                child: Text(
                                                  "\$ 2,70,000",
                                                  style: TextStyle(
                                                      fontFamily:
                                                          AppFont.poppinsFont,
                                                      color: Color(
                                                          0xff1D3A74), // Text color when selected,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(left: 15),
                                          child: Text(
                                            "Do you want to go with the full payment?",
                                            style: TextStyle(
                                                fontFamily: AppFont.poppinsFont,
                                                color: Colors
                                                    .black, // Text color when selected,
                                                fontWeight: FontWeight.w400,
                                                fontSize: 15),
                                          ),
                                        ),
                                        Container(
                                          // color: Colors.red,
                                          child: ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: actionPurposeController
                                                .paymentTypeList.length,
                                            physics:
                                                NeverScrollableScrollPhysics(),
                                            itemBuilder: (context, index) {
                                               option1 =
                                                  actionPurposeController
                                                      .paymentTypeList[index];
                                              return Obx(
                                                () => Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    CheckboxListTile(
                                                      title: Text(
                                                        option1,
                                                        style: TextStyle(
                                                            fontFamily: AppFont
                                                                .poppinsFont,
                                                            color: Colors
                                                                .black, // Text color when selected
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize: 12),
                                                      ),
                                                      subtitle: actionPurposeController
                                                                  .paymentTypeList[index]
                                                                  .toString() ==
                                                              "I don’t have full amount available."
                                                          ? Text("true")
                                                          : Text("false"),
                                                      value: actionPurposeController
                                                          .selectBuyerPaymentGrowable
                                                          .contains(option1),
                                                      onChanged: (bool? value) {
                                                        if (value != null) {
                                                          actionPurposeController
                                                              .togglebuyerPayment(
                                                                  option1);
                                                          print("option......${option1}");

                                                          // print("value");
                                                          // print();
                                                        }
                                                      },
                                                      controlAffinity:
                                                          ListTileControlAffinity
                                                              .leading,
                                                      checkColor:
                                                          Color(0xff1D3A74),
                                                      selectedTileColor: Colors
                                                          .transparent, // Ensure the selected tile color is transparent
                                                      activeColor: Colors
                                                          .white, // Adjust the color as needed
                                                      checkboxShape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                5), // Adjust border radius as needed
                                                        side: BorderSide(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      side: BorderSide(
                                                          color: Colors.black),
                                                      dense: true,
                                                    ),

                                                    // if ()
                                                    // Text(),
                                                    // Obx(()=>
                                                    //    actionPurposeController
                                                    //       .selectedItems
                                                    //       .contains(
                                                    //       option) =="I don’t have full amount available."?
                                                    //     Text("shujja"):Container(),
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                        option1 ==
                                        "I don’t have full amount available."
                                        ? Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                height: 24,
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding:
                                                        EdgeInsets.only(
                                                            left: 15),
                                                    child: Text(
                                                      "Property Price",
                                                      style: TextStyle(
                                                          fontFamily: AppFont
                                                              .poppinsFont,
                                                          color:
                                                              Colors.black,
                                                          fontWeight:
                                                              FontWeight
                                                                  .w700,
                                                          fontSize: 15),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.only(
                                                              left: 15,
                                                              top: 11.5,
                                                              bottom: 11.5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8),
                                                          color:
                                                              Colors.white),
                                                      child: Text(
                                                        "\$ 10,000",
                                                        style: TextStyle(
                                                            fontFamily: AppFont
                                                                .poppinsFont,
                                                            color: Color(
                                                                0xff1D3A74), // Text color when selected,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400,
                                                            fontSize: 15),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding:
                                                        EdgeInsets.only(
                                                            left: 15),
                                                    child: Text(
                                                      "Mortgage Amount",
                                                      style: TextStyle(
                                                          fontFamily: AppFont
                                                              .poppinsFont,
                                                          color:
                                                              Colors.black,
                                                          fontWeight:
                                                              FontWeight
                                                                  .w700,
                                                          fontSize: 15),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.only(
                                                              left: 15,
                                                              top: 11.5,
                                                              bottom: 11.5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8),
                                                          color:
                                                              Colors.white),
                                                      child: Text(
                                                        "\$ 10,000",
                                                        style: TextStyle(
                                                            fontFamily: AppFont
                                                                .poppinsFont,
                                                            color: Color(
                                                                0xff1D3A74), // Text color when selected,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400,
                                                            fontSize: 15),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding:
                                                        EdgeInsets.only(
                                                            left: 15),
                                                    child: Text(
                                                      "Re-payment period",
                                                      style: TextStyle(
                                                          fontFamily: AppFont
                                                              .poppinsFont,
                                                          color: Color(
                                                              0xff091130),
                                                          fontWeight:
                                                              FontWeight
                                                                  .w400,
                                                          fontSize: 15),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Obx(
                                                () => Container(
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius
                                                              .circular(8)),
                                                  child:
                                                      DropdownButtonFormField<
                                                          String>(
                                                    value: actionPurposeController
                                                                .selectedPaymentValue
                                                                .value ==
                                                            ""
                                                        ? null
                                                        : actionPurposeController
                                                            .selectedPaymentValue
                                                            .value,
                                                    onChanged:
                                                        (String? newValue) {
                                                      actionPurposeController
                                                          .setSelectedPaymentValue(
                                                              newValue
                                                                  .toString());
                                                    },
                                                    items: fruitList.map<
                                                            DropdownMenuItem<
                                                                String>>(
                                                        (value) {
                                                      return DropdownMenuItem<
                                                          String>(
                                                        value: value,
                                                        child: Text(value),
                                                      );
                                                    }).toList(),
                                                    icon: SvgPicture.asset(
                                                        AppIcons
                                                            .icDropDownArrowDown),
                                                    decoration:
                                                        InputDecoration(
                                                      hintText:
                                                          "Select Payment",
                                                      hintStyle: TextStyle(
                                                          fontSize: 12,
                                                          fontFamily: AppFont
                                                              .poppinsFont,
                                                          fontWeight:
                                                              FontWeight
                                                                  .w400),
                                                      contentPadding:
                                                          EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      10),
                                                      enabledBorder:
                                                          InputBorder.none,
                                                      focusedBorder:
                                                          InputBorder.none,
                                                      errorBorder:
                                                          InputBorder.none,
                                                      border:
                                                          OutlineInputBorder(),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.only(left: 15),
                                                child: Text(
                                                  "Do you want to partner with other people ?",
                                                  style: TextStyle(
                                                      fontFamily: AppFont.poppinsFont,
                                                      color: Colors
                                                          .black, // Text color when selected,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 15),
                                                ),
                                              ),
                                              Container(
                                                // color: Colors.red,
                                                child: ListView.builder(
                                                  shrinkWrap: true,
                                                  itemCount: actionPurposeController
                                                      .wantPartnerList.length,
                                                  physics:
                                                  NeverScrollableScrollPhysics(),
                                                  itemBuilder: (context, index) {
                                                   var option2 = actionPurposeController
                                                        .wantPartnerList[index];
                                                    return Obx(
                                                          () => Column(
                                                        mainAxisSize:
                                                        MainAxisSize.min,
                                                        children: [
                                                          CheckboxListTile(
                                                            title: Text(
                                                              option2,
                                                              style: TextStyle(
                                                                  fontFamily: AppFont
                                                                      .poppinsFont,
                                                                  color: Colors
                                                                      .black, // Text color when selected
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  fontSize: 12),
                                                            ),
                                                            value:
                                                            actionPurposeController
                                                                .selectPartner
                                                                .contains(option2),
                                                            onChanged: (bool? value) {
                                                              if (value != null) {
                                                                actionPurposeController
                                                                    .togglePartnerSerch(
                                                                    option2);
                                                              }
                                                            },
                                                            controlAffinity:
                                                            ListTileControlAffinity
                                                                .leading,
                                                            checkColor:
                                                            Color(0xff1D3A74),
                                                            selectedTileColor: Colors
                                                                .transparent, // Ensure the selected tile color is transparent
                                                            activeColor: Colors
                                                                .white, // Adjust the color as needed
                                                            checkboxShape:
                                                            RoundedRectangleBorder(
                                                              borderRadius:
                                                              BorderRadius.circular(
                                                                  5), // Adjust border radius as needed
                                                              side: BorderSide(
                                                                  color:
                                                                  Colors.black),
                                                            ),
                                                            side: BorderSide(
                                                                color: Colors.black),
                                                            dense: true,
                                                          ),

                                                          // if ()
                                                          // Text(),
                                                          // Obx(()=>
                                                          //    actionPurposeController
                                                          //       .selectedItems
                                                          //       .contains(
                                                          //       option) =="I don’t have full amount available."?
                                                          //     Text("shujja"):Container(),
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Divider(),
                                              SizedBox(height: 10,),

                                              Container(
                                                padding: EdgeInsets.only(left: 15),
                                                child: Text(
                                                  "Do you want to partner with other people ?",
                                                  style: TextStyle(
                                                      fontFamily: AppFont.poppinsFont,
                                                      color: Colors
                                                          .black, // Text color when selected,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 15),
                                                ),
                                              ),

                                            ],
                                          )
                                        : Text("")
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : Text("shahrukh"));
                  },
                ),
              ),
              // Expanded(child: Container()),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: AppButton(
                    titleText: 'Sign Up',
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                    onPressed: () {
                      if (actionPurposeController.currentIndex == 0) {
                        actionPurposeController.goToPage(1);
                      } else if (actionPurposeController.currentIndex == 1)
                        actionPurposeController.goToPage(2);
                    },
                    buttonColor: AppColors.primaryColor,
                    textColor: AppColors.white),
              ),
              SizedBox(
                height: 34,
              )
            ],
          )),
    );
  }
}

class StepperComponent extends StatelessWidget {
  final int index;
  final bool isLast;
  final VoidCallback onTap;
  final String imagePath; // Add this parameter to hold the image path
  final subTittle;

  const StepperComponent(
      {required this.index,
      required this.onTap,
      this.isLast = false,
      required this.imagePath,
      required this.subTittle});

  @override
  Widget build(BuildContext context) {
    final ActionPurposeController actionPurposeController =
        Get.find<ActionPurposeController>();

    return Obx(
      () => isLast
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    GestureDetector(
                      onTap: onTap,
                      child: Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: actionPurposeController.currentIndex.value >=
                                  index
                              ? Color(0xff1F7FFD)
                              : Color(0xffE9E9E9),
                          border: Border.all(
                            color: actionPurposeController.currentIndex.value >=
                                    index + 1
                                ? Color(0xff1F7FFD)
                                : Color(0xffE9E9E9),
                          ),
                        ),
                        child: Container(
                          height: 40,
                          width: 40,
                          child: SvgPicture.asset(
                            imagePath,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Text(
                  subTittle,
                  style: TextStyle(
                      fontFamily: AppFont.poppinsFont,
                      color: actionPurposeController.currentIndex.value >=
                              index + 1
                          ? Colors.black
                          : Color(0xff153050),
                      fontWeight: FontWeight.w600,
                      fontSize: 11),
                )
                // Text(subTittle)
              ],
            )
          : Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: onTap,
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: actionPurposeController.currentIndex.value >=
                                    index
                                ? Color(0xff1F7FFD)
                                : Color(0xffE9E9E9),
                            border: Border.all(
                              color:
                                  actionPurposeController.currentIndex.value >=
                                          index + 1
                                      ? Color(0xff1F7FFD)
                                      : Color(0xffE9E9E9),
                            ),
                          ),
                          child: SvgPicture.asset(
                            imagePath,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: actionPurposeController.currentIndex.value >=
                                    index + 1
                                ? Color(0xff1F7FFD)
                                : Color(0xffE9E9E9),
                          ),
                          height: 10,
                        ),
                      )
                    ],
                  ),
                  Text(
                    subTittle,
                    style: TextStyle(
                        fontFamily: AppFont.poppinsFont,
                        color: actionPurposeController.currentIndex.value >=
                                index + 1
                            ? Colors.black
                            : Color(0xff153050),
                        fontWeight: FontWeight.w600,
                        fontSize: 11),
                  )
                ],
              ),
            ),
    );
  }
}
