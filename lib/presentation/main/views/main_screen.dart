import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../global/app_theme/app_colors.dart';
import '../../about_us_screen/about_us_screen.dart';
import '../../fav_screen/favorite_screen.dart';
import '../../term_and_condition_screen/term_and_screen.dart';
import '../controllers/main_controller.dart';
import 'drawer_screen.dart';
import 'main_view.dart';

late Size mq;

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final MainController _mainLandingController = Get.put(MainController());
  late double xOffset = 270;
  late double yOffset = 155;
  late double scaleFactor = .6;
  bool isDragging = false;
  late bool isDrawerOpen = false;
  DrawerItem item1 = DrawerItems.profile;
  @override
  void initState() {
    // TODO: implement initState
    closeDrawer();
    super.initState();
  }

  openDrawer() {
    setState(() {
      xOffset = 270;
      yOffset = 240;
      scaleFactor = .6;
      isDrawerOpen = true;
    });
  }

  closeDrawer() {
    setState(() {
      xOffset = 0;
      yOffset = 0;
      scaleFactor = 1;
      isDrawerOpen = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.appBgColor,
        // body:
        // Obx(() => AnimatedContainer(
        //   duration: const Duration(milliseconds: 250),
        //   decoration: BoxDecoration(
        //     color: Colors.white,
        //     borderRadius: BorderRadius.only(
        //       topLeft: _mainLandingController.isToggleDrawer.value ? Radius.zero : const Radius.circular(25),
        //       bottomLeft: _mainLandingController.isToggleDrawer.value ? Radius.zero : const Radius.circular(25),
        //     ),
        //   ),
        //   transform: Matrix4.translationValues(
        //     _mainLandingController.tranX.value,
        //     _mainLandingController.tranY.value,
        //     0,
        //   )..scale(_mainLandingController.scaleFactory.value),
        //   child: Container(
        //     color: Colors.lightBlue,
        //     child: Column(
        //       children: [
        //         // Your existing widget tree
        //       ],
        //     ),
        //   ),
        // )),

        body: Stack(
          children: [
            buildDrawer(),
            BuildPage()

            // Positioned(
            //   top: 0,
            //   bottom: 0,
            //   left: 0,
            //   right: 0,
            //   child: AnimatedBuilder(
            //     animation: _mainLandingController.model,
            //     builder: (context, child) {
            //       return Stack(
            //         children: [
            //
            //           Obx(
            //             () => Positioned(
            //               top: 45,
            //               left: 0,
            //               right: 0,
            //               bottom: 50,
            //               child: IndexedStack(
            //                 index: _mainLandingController.currentIndex.value,
            //                 children: [
            //                   FeedsScreen(),
            //                   StatsScreen(),
            //                   FavouriteScreen(),
            //                   SettingScreen(),
            //                   SettingScreen(),
            //                 ],
            //               ),
            //             ),
            //           ),

            ,

            //         ],
            //       );
            //     },
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  BuildPage() {
    return WillPopScope(
      onWillPop: () async {
        if (isDrawerOpen) {
          closeDrawer();
          return false;
        } else {
          return true;
        }
      },
      child: GestureDetector(
        onTap: closeDrawer,
        onHorizontalDragStart: (details) => isDragging = true,
        onHorizontalDragUpdate: (details) {
          if (!isDragging) return;
          const delta = 1;
          if (details.delta.dx > delta) {
            openDrawer();
          } else if (details.delta.dx < -delta) {
            closeDrawer();
          }
          isDragging = false;
        },
        child: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          transform: Matrix4.translationValues(xOffset, yOffset, 0)
            ..scale(scaleFactor),
          child: AbsorbPointer(
            absorbing: isDrawerOpen,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(isDrawerOpen ? 20 : 0),
              child: Container(
                color: isDrawerOpen
                    ? Colors.black.withOpacity(.1)
                    : AppColors.appBgColor,
                child: MainView(
                  isDrawerOpen: isDrawerOpen,
                  openDrawer: openDrawer,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // Widget getDrawerPage() {
  //   if (item1 == DrawerItems.profile) {
  //     return
  //   }
  //   else if (item1 == DrawerItems.fav) {
  //     return FavrioteScreen();
  //   } else if (item1 == DrawerItems.aboutUs) {
  //     return AboutUsScreen();
  //   }
  //   else if (item1 == DrawerItems.termAndConditions) {
  //     return TermAndConditionsScreen();
  //   } else {
  //     return MainView(isDrawerOpen: isDrawerOpen,openDrawer: openDrawer,);
  //
  //   }
  // }

  Widget buildDrawer() => Container(
        // width: xOffset,
        child: DrawerScreen(
          closeDrawer: closeDrawer,
          onSelectedItem: (item) {
            if (item1 == DrawerItems.logout) {
              print("logout");
            } else {
              setState(() => this.item1 = item);
              closeDrawer();
            }
          },
        ),
      );
}
