import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import '../../../constants/app_assets/app_font.dart';
import '../../../constants/app_assets/app_icons.dart';
import '../../../global/app_theme/app_colors.dart';
import '../../../global/app_widgets/app_appbar.dart';
import '../../account_screen/accout_screen.dart';
import '../controllers/main_controller.dart';
import '../tabs/favourite/views/favourite_screen.dart';
import '../tabs/feeds/views/feeds_screen.dart';
import '../tabs/setting/views/setting_screen.dart';
import '../tabs/stats/views/stats_screen.dart';
late Size mq;

class MainView extends StatelessWidget {
   MainView({super.key,required this.isDrawerOpen ,required this.openDrawer});
  final MainController _mainLandingController = Get.put(MainController());
final bool isDrawerOpen;
   final VoidCallback openDrawer;

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);

    return Column(
      children: [
        Obx(
              () => _mainLandingController.currentIndex.value == 0
              ? AppAppBar(
            isDrawerOpen: isDrawerOpen,
            title: 'feeds',
            openDrawer: openDrawer,
          )
              : _mainLandingController.currentIndex.value == 1
              ? AppAppBar(
            isDrawerOpen: isDrawerOpen,
            title: 'property',
            openDrawer: openDrawer,
          )
              : _mainLandingController.currentIndex.value == 2
              ? AppAppBar(
            isDrawerOpen: isDrawerOpen,
            title: 'chat',
            openDrawer: openDrawer,
          )
              : _mainLandingController.currentIndex.value == 3
              ? AppAppBar(
            isDrawerOpen: isDrawerOpen,
            title: 'dashboard',
            openDrawer: openDrawer,
          )
              : AppAppBar(
            isDrawerOpen: isDrawerOpen,
            title: 'account',
            openDrawer: openDrawer,
          ),
        ),
        Obx(
              () => Expanded(
            child: IndexedStack(
              index: _mainLandingController.currentIndex.value,
              children: [
                FeedsScreen(),
                StatsScreen(),
                FavouriteScreen(),
                SettingScreen(),
                AccountScreen(),
              ],
            ),
          ),
        ),
        // Expanded(child
        //     : getDrawerPage()),
        Obx(
              () => SizedBox(
            height: mq.height * 0.11,
            child: ClipRRect(
              // borderRadius: const BorderRadius.only(
              //   topLeft: Radius.circular(25),
              //   topRight: Radius.circular(25),
              // ),
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(color: AppColors.black.withOpacity(0.3)),
                    bottom: BorderSide.none,
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 30,
                      offset: Offset(0, 10),
                    ),
                  ],
                ),
                child: BottomNavigationBar(
                  items: [
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            AppIcons.homeBottomBarIcon,
                            color:
                            _mainLandingController.currentIndex.value == 0
                                ? AppColors.primaryColor
                                : AppColors.grey.withOpacity(.50),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Feeds",
                            style: TextStyle(
                                fontFamily: AppFont.poppinsFont,
                                color: _mainLandingController
                                    .currentIndex.value ==
                                    0
                                    ? AppColors.primaryColor
                                    : AppColors.grey.withOpacity(.50),
                                fontSize: 12),
                          )
                        ],
                      ),
                      label: 'feeds',
                    ),
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            AppIcons.homeBottomBarIcon,
                            color:
                            _mainLandingController.currentIndex.value == 1
                                ? AppColors.primaryColor
                                : AppColors.grey.withOpacity(.50),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Property",
                            style: TextStyle(
                                fontFamily: AppFont.poppinsFont,
                                color: _mainLandingController
                                    .currentIndex.value ==
                                    1
                                    ? AppColors.primaryColor
                                    : AppColors.grey.withOpacity(.50),
                                fontSize: 12),
                          )
                        ],
                      ),
                      label: 'property',
                    ),
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            AppIcons.homeBottomBarIcon,
                            color:
                            _mainLandingController.currentIndex.value == 2
                                ? AppColors.primaryColor
                                : AppColors.grey.withOpacity(.50),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Chat",
                            style: TextStyle(
                                fontFamily: AppFont.poppinsFont,
                                color: _mainLandingController
                                    .currentIndex.value ==
                                    2
                                    ? AppColors.primaryColor
                                    : AppColors.grey.withOpacity(.50),
                                fontSize: 12),
                          )
                        ],
                      ),
                      label: 'chat',
                    ),
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            AppIcons.homeBottomBarIcon,
                            color:
                            _mainLandingController.currentIndex.value == 3
                                ? AppColors.primaryColor
                                : AppColors.grey.withOpacity(.50),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Dashboard",
                            style: TextStyle(
                                fontFamily: AppFont.poppinsFont,
                                color: _mainLandingController
                                    .currentIndex.value ==
                                    3
                                    ? AppColors.primaryColor
                                    : AppColors.grey.withOpacity(.50),
                                fontSize: 12),
                          )
                        ],
                      ),
                      label: 'dashboard',
                    ),
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            AppIcons.homeBottomBarIcon,
                            color:
                            _mainLandingController.currentIndex.value == 4
                                ? AppColors.primaryColor
                                : AppColors.grey.withOpacity(.50),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Setting",
                            style: TextStyle(
                                fontFamily: AppFont.poppinsFont,
                                color: _mainLandingController
                                    .currentIndex.value ==
                                    4
                                    ? AppColors.primaryColor
                                    : AppColors.grey.withOpacity(.50),
                                fontSize: 12),
                          )
                        ],
                      ),
                      label: 'Setting',
                    ),
                  ],
                  currentIndex: _mainLandingController.currentIndex.value,

                  selectedItemColor: Colors.white,
                  onTap: _mainLandingController.onItemTapped,

                  backgroundColor: AppColors.white,
                  // currentIndex: _selectedPageIndex,
                  // onTap: _selectPage,
                  elevation: 40,
                  showSelectedLabels: false,
                  showUnselectedLabels: false,
                  type: BottomNavigationBarType.fixed,
                  // unselectedItemColor: const Color(0xFF2E2F41),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
