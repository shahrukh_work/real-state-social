import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:pool_real_estate/config/routes/app_routes.dart';
import 'package:pool_real_estate/constants/app_assets/app_icons.dart';

class DrawerScreen extends StatelessWidget {
  DrawerScreen({super.key, required this.onSelectedItem,required this.closeDrawer});
  final ValueChanged<DrawerItem> onSelectedItem;
  final void Function()? closeDrawer;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 58, bottom: 59, left: 20, right: 20),
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                Color(0xffA5CCFF),
                Color(0xff7DA3F9),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(

                  onTap:closeDrawer,
                    child: Image.asset("assets/images/ic_arrow_back.png"),
                ),
                SvgPicture.asset("assets/images/ic_home_icon_image.svg"),
                Container()
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 112),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              leading: Container(
                height: 16,
                width: 16,
                child: SvgPicture.asset(AppIcons.icProfile),
              ),
              trailing: SvgPicture.asset(AppIcons.icIconForward),
              title: Text("Profile"),
              onTap: () {
                Get.toNamed(profileScreen);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 112),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              leading: Container(
                height: 16,
                width: 16,
                child: SvgPicture.asset(
                  AppIcons.icFavouriteImage,
                ),
              ),
              trailing: SvgPicture.asset(AppIcons.icIconForward),
              title: Text("Favorite"),
              onTap: () {
                Get.toNamed(favoriteScreen);
              },
            ),
          ),    Padding(
            padding: const EdgeInsets.only(right: 112),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              leading: Container(
                height: 16,
                width: 16,
                child: SvgPicture.asset(
                  AppIcons.icAboutUs,
                ),
              ),
              trailing: SvgPicture.asset(AppIcons.icIconForward),
              title: Text("About us"),
              onTap: () {
                Get.toNamed(aboutUsScreenDrawer);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 112),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              leading: Container(
                height: 16,
                width: 16,
                child: SvgPicture.asset(
                  AppIcons.icTermAndConditions,
                ),
              ),
              trailing: SvgPicture.asset(AppIcons.icIconForward),
              title: Text("Term & Condition"),
              onTap: () {
                Get.toNamed(termAndConditionScreen);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 112),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              leading: Container(
                height: 16,
                width: 16,
                child: SvgPicture.asset(
                  AppIcons.icLogoutImage,
                ),
              ),
              trailing: SvgPicture.asset(AppIcons.icIconForward),
              title: Text("Logout"),
              onTap: () {},
            ),
          ),         Padding(
            padding: const EdgeInsets.only(right: 112),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              leading: Container(
                height: 16,
                width: 16,
                child: SvgPicture.asset(
                  AppIcons.icDeleteIcon,
                ),
              ),
              trailing: SvgPicture.asset(AppIcons.icIconForward),
              title: Text("Delete account"),
              onTap: () {},
            ),
          ),
          // buildDrawerItem(context)
        ],
      ),
    );
  }

  Widget buildDrawerItem(BuildContext context) => Padding(
        padding: const EdgeInsets.only(right: 112),
        child: ListView.separated(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: DrawerItems.all.length,
          itemBuilder: (BuildContext context, int index) {
            final item = DrawerItems.all[index];
            return ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              leading: Container(
                height: 16,
                width: 16,
                child: SvgPicture.asset(item.icon),
              ),
              trailing: SvgPicture.asset(AppIcons.icIconForward),
              title: Text(item.tittle),
              onTap: () => onSelectedItem(item),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.only(left: 25, right: 25),
              child: Divider(),
            ); // Add a Divider between each ListTile
          },
        ),
      );
}

// buildDraweItem(BuildContext context) => Padding(
//   padding: const EdgeInsets.only(right: 112),
//   child: Column(
//       children: DrawerItems.all
//           .map(
//             (item) => ListTile(
//               contentPadding: EdgeInsets.symmetric(horizontal: 24,vertical: 8),
//               leading: Container(
//                 height: 16,
//                 width: 16,
//                 child: SvgPicture.asset(item.icon),
//               ),
//               trailing: SvgPicture.asset(AppIcons.icIconForward),
//               title: Text(item.tittle),
//               onTap: () {},
//
//             ),
//           )
//           .toList(),
//   ),
// );

class DrawerItem {
  final String tittle;
  final String icon;
  DrawerItem({required this.tittle, required this.icon});
}

class DrawerItems {
  // static var mainS = DrawerItem(
  //   tittle: 'mainScree,',
  //   icon: AppIcons.icProfile,
  // );
  static var profile = DrawerItem(
    tittle: 'Profile',
    icon: AppIcons.icProfile,
  );
  static var fav = DrawerItem(
    tittle: 'Favourite',
    icon: AppIcons.icFavouriteImage,
  );
  static var aboutUs = DrawerItem(
    tittle: 'About us',
    icon: AppIcons.icAboutUs,
  );
  static var termAndConditions = DrawerItem(
    tittle: 'Term & condition',
    icon: AppIcons.icTermAndConditions,
  );
  static var logout = DrawerItem(
    tittle: 'Logout',
    icon: AppIcons.icLogoutImage,
  );
  static var deleteAccount = DrawerItem(
    tittle: 'Delete account',
    icon: AppIcons.icDeleteIcon,
  );

  static final List<DrawerItem> all = [
    // mainS,
    profile,
    fav,
    aboutUs,
    termAndConditions,
    logout,
    deleteAccount
  ];
}
