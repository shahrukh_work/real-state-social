import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FullScreenImage extends StatelessWidget {
  final String image;

  const FullScreenImage({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black, // Background color for the full screen image
      body: GestureDetector(
        onTap: () {
          Navigator.of(context).pop(); // Dismiss the full screen image when tapped
        },
        child: Center(
          child: Image.asset(
            image,
            fit: BoxFit.contain, // Adjust the image fitting as required
          ),
        ),
      ),
    );
  }
}
