import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';
import 'package:pool_real_estate/presentation/main/tabs/feeds/models/feeds_posts_model.dart';

import '../../../../../constants/app_assets/app_icons.dart';
import '../../../../../global/app_theme/app_colors.dart';
import '../../../../../global/app_widgets/app_text_widget.dart';
import '../../../../../main.dart';

class FeedsMediaPostWidget extends StatelessWidget {
  final FeedsPostModel feedsPostModel;
  FeedsMediaPostWidget({Key? key, required this.feedsPostModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              contentPadding: EdgeInsets.zero,
              leading: Image(image: AssetImage(AppImages.profilePic)),
              title: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTextWidget(
                    text: feedsPostModel.userName,
                    color: AppColors.black,
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                  AppTextWidget(
                    text: 'post: ${feedsPostModel.postCreatedAt}',
                    color: AppColors.black,
                    fontSize: 9,
                    fontWeight: FontWeight.w400,
                  ),
                ],
              ),
              trailing: AppTextWidget(
                text: 'Follow',
                color: AppColors.primaryColor,
                fontSize: 13,
                fontWeight: FontWeight.w500,
              ),
            ),
            Visibility(
              visible: feedsPostModel.postText != null,
              child: AppTextWidget(
                text: '${feedsPostModel.postText}',
                color: AppColors.appGrey,
                fontSize: 13,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(
              height: mq.height * 0.01,
            ),
            feedsPostModel.mediaList != null
                ? Container(
                    height: mq.height * 0.3,
                    // Adjust height as needed
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: feedsPostModel.mediaList!.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onLongPress: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pop(); // Dismiss the dialog when tapped
                                    },
                                    child: AnimatedContainer(
                                      duration: Duration(
                                          milliseconds:
                                              800), // Adjust duration as needed
                                      curve: Curves.bounceIn,
                                      // Adjust curve as needed
                                      // Animate width and height to full screen size
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(20),
                                        child: AnimatedOpacity(
                                          duration: Duration(
                                              milliseconds:
                                                  1000), // Adjust opacity animation duration
                                          curve: Curves
                                              .bounceIn, // Adjust opacity animation curve
                                          opacity: 1.0,
                                          child: Image.asset(
                                            feedsPostModel.mediaList![index],
                                            fit: BoxFit
                                                .fill, // Adjust the image fitting as required
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: Image.asset(
                                feedsPostModel.mediaList![index],
                                height: mq.height * 0.1,
                                width: mq.width * 0.4,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  )
                : SizedBox(),
            SizedBox(
              height: mq.height * 0.01,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      AppIcons.likeIcon,
                      color: AppColors.appGrey,
                    ),
                    SizedBox(
                      width: mq.width * 0.01,
                    ),
                    AppTextWidget(
                      text: "${feedsPostModel.postLikes.toString()}k",
                      color: AppColors.appGrey,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                ),
                Row(
                  children: [
                    SvgPicture.asset(
                      AppIcons.commentIcon,
                      color: AppColors.appGrey,
                    ),
                    SizedBox(
                      width: mq.width * 0.01,
                    ),
                    AppTextWidget(
                      text: "${feedsPostModel.postComments.toString()}k",
                      color: AppColors.appGrey,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                ),
                Row(
                  children: [
                    SvgPicture.asset(
                      AppIcons.shareIcon,
                      color: AppColors.appGrey,
                    ),
                    SizedBox(
                      width: mq.width * 0.01,
                    ),
                    AppTextWidget(
                      text: "${feedsPostModel.postShares.toString()}k",
                      color: AppColors.appGrey,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
