class FeedsPostModel {
  final String userProfilePic;
  final String postCreatedAt;
  final String userName;
  final String? postText;
  final double? postLikes;
  final double? postComments;
  final double? postShares;
  final List<String>? mediaList;

  FeedsPostModel(
     {
    required this.userProfilePic,
    required this.postCreatedAt,
    required this.userName,
       this.postLikes,
       this.postComments,
       this.postShares,
       this.postText,
       this.mediaList,
  });
}
