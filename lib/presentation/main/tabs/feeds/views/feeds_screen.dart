import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pool_real_estate/presentation/main/tabs/feeds/widgets/feeds_media_post_widget.dart';
import '../../../../../global/app_theme/app_colors.dart';
import '../../../../../main.dart';
import '../controllers/feeds_controller.dart';

class FeedsScreen extends StatelessWidget {
  FeedsScreen({super.key});
  final feedsPostsController = Get.put(FeedsController());

  @override
  Widget build(BuildContext context) {
    print(feedsPostsController.feedsPostsList.length);
    mq = MediaQuery.sizeOf(context);
    return Column(
          children: [
            Expanded(
                child: ListView.custom(
              physics: BouncingScrollPhysics(),
              childrenDelegate: SliverChildBuilderDelegate(
                (context, index) {
                  return FeedsMediaPostWidget(
                    feedsPostModel: feedsPostsController.feedsPostsList[index],
                  );
                },
                childCount: feedsPostsController.feedsPostsList.length,
              ),
            )),
            SizedBox(
              height: mq.height * 0.05,
            ),
          ],
        );
  }
}
