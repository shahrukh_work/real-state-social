
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';
import 'package:pool_real_estate/presentation/main/tabs/feeds/models/feeds_posts_model.dart';

class FeedsController extends GetxController {
  final RxList<FeedsPostModel> feedsPostsList = <FeedsPostModel>[].obs;
  // final RxList<BudgetModel> favoriteBudgetList = <BudgetModel>[].obs;
  // final Rx<BudgetModel?> singleBudgetModel = Rx<BudgetModel?>(null);

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    loadPosts();
    // loadBudgets();
  }

  loadPosts(){
    feedsPostsList.value = [
      FeedsPostModel(
        userProfilePic: AppImages.profilePic,
        postCreatedAt: '1 min ago',
        userName: 'Alice Smith',
        postText: 'Another sample post text.',
        postLikes: 20,
        postComments: 8,
        postShares: 3,
        mediaList: [AppImages.socialMediaPostFive, AppImages.socialMediaPostSix,AppImages.socialMediaPostFour], // Sample image paths
      ),
      FeedsPostModel(
        userProfilePic: AppImages.profilePic,
        postCreatedAt: 'yesterday',
        userName: 'Aliza',
        postText: 'Another sample post text.Another sample post text.Another sample post text.Another sample post text.Another sample post text.Another sample post text.',
        postLikes: 2.5,
        postComments: 8,
        postShares: 3.2, // Sample image paths
      ),
      FeedsPostModel(
        userProfilePic: AppImages.profilePic,
        postCreatedAt: '10 min ago',
        userName: 'John Doe',
        postText: 'This is a sample post text.',
        postLikes: 10,
        postComments: 5,
        postShares: 2,
        mediaList: [AppImages.socialMediaPostTwo, AppImages.socialMediaImage,AppImages.socialMediaPostTwo, AppImages.socialMediaImage,], // Sample image paths
      ),
      FeedsPostModel(
        userProfilePic: AppImages.profilePic,
        postCreatedAt: '1 min ago',
        userName: 'Alice Smith',
        postText: 'Another sample post text.',
        postLikes: 20,
        postComments: 8,
        postShares: 3,
        mediaList: [AppImages.socialMediaImage, AppImages.socialMediaPostTwo,], // Sample image paths
      ),

      FeedsPostModel(
        userProfilePic: AppImages.profilePic,
        postCreatedAt: '1 min ago',
        userName: 'Alice Smith',
        postText: 'Another sample post text.',
        postLikes: 20,
        postComments: 8,
        postShares: 3,
        mediaList: [AppImages.socialMediaPostThree, AppImages.socialMediaPostTwo,AppImages.socialMediaPostFour], // Sample image paths
      ),

      // Add more posts as needed
    ];
  }

}
