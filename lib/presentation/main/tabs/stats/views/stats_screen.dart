import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../config/routes/app_routes.dart';
import '../../../../../main.dart';
import '../controllers/stats_details_controller.dart';

class StatsScreen extends StatelessWidget {
  StatsScreen({super.key});
  final StatsDetailsController statsDetailsController =
      Get.put(StatsDetailsController());

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: GetBuilder<StatsDetailsController>(
          builder: (_) => Container(
                // color: Colors.red,

                child: Stack(
                  children: [
                    GridView.builder(
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 0, // childAspectRatio: 2.0, // Aspect ratio will be calculated dynamically

                          mainAxisExtent: 260,
                          childAspectRatio: 100),
                      itemCount: statsDetailsController.imageList.length,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (context, index) {
                        var image = statsDetailsController.imageList[index];
                        double height;
                        if (index == 1 || index == 4) {
                          height = 150.0; // Height for the 2nd and 5th items
                        } else {
                          height = 200.0; // Default height for other items
                        }
                        return InkWell(
                          onTap: () {
                            Get.toNamed(imageDetailAndGalleryScreen);
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                topLeft: Radius.circular(10),
                              ),
                            ),
                            child: Container(
                              height: height,
                              child: Stack(
                                children: [
                                  Positioned(
                                    bottom: 0,
                                    top: 0,
                                    left: 0,
                                    right: 0,
                                    child: Image.asset(
                                      image.imageUrl,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 10,
                                    left: 15,
                                    child: Text(
                                      image.title,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                    Positioned(
                        bottom: 50,
                        right: 10,
                        child:
                            Image.asset("assets/images/ic_add_icon_button.png"))
                  ],
                ),
              )),
    );
  }
}

// class PieData {
//   PieData(this.xData, this.yData, this.percentage, [this.text]);
//   final String xData;
//   final num yData;
//   final String? percentage; // Add this field to store the percentage value
//   String? text;
// }
