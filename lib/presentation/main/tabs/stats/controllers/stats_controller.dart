import 'dart:math';

import 'package:get/get.dart';

import '../models/stats_model.dart';

class StatsController extends GetxController{
  final List<StatsModel> statsList = List.generate(10, (index) {
    final creationDate = DateTime.now().subtract(Duration(days: index)); // Example: creating stats with creation dates in the past
    return StatsModel(creationDate: creationDate)
      ..id!.value = index + 1
      ..name!.value = 'Stat ${index + 1}'
      ..amount!.value = (index + 1) * 100.0 // Dummy amount, you can generate as needed
      ..percentage!.value = Random().nextDouble() * 100; // Random percentage between 0 and 100
  });
}