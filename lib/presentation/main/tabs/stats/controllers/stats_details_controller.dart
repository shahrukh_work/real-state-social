import 'package:get/get.dart';

import '../../../../../constants/app_assets/app_images.dart';

class StatsDetailsController extends GetxController{



  final imageList = [
    ImageModel(
      imageUrl: AppImages.icFeedImageOne,
      title: "House",
      height: 260

    ),   ImageModel(
      imageUrl: AppImages.icFeedImageTwo,
      title: "Villa",
        height: 200

    ),
    ImageModel(
      imageUrl: AppImages.icFeedImageThree,
      title: "Soceity",height: 260,

    ),   ImageModel(
      imageUrl: AppImages.icFeedImageTwo,
      title: "Villa",
      height: 200,
    ),
    ImageModel(
      imageUrl: AppImages.icFeedImageOne,
      title: "House",
      height: 260
    ),   ImageModel(
      imageUrl: AppImages.icFeedImageThree,
      title: "Soceity",
      height: 260,


    ),
    ImageModel(
      imageUrl: AppImages.icFeedImageOne,
      title: "House",
      height: 200
    ),   ImageModel(
      imageUrl: AppImages.icFeedImageThree,
      title: "Villa",
      height: 260
    ),


  ].obs;

}

class ImageModel {
  final String imageUrl;
  final String title;
  final double height;


  ImageModel({required this.imageUrl, required this.title,required this.height});
}