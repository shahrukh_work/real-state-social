import 'package:get/get.dart';

class StatsModel extends GetxController {
  final RxInt? id = 0.obs;
  final RxString? name = ''.obs;
  final RxDouble? amount = 0.0.obs;
  final RxDouble? percentage = 0.0.obs;
  final RxString? createdAt = ''.obs; // New property for creation date
  final RxString? updatedAt = ''.obs; // New property for creation date

  StatsModel({required DateTime creationDate}) {
    // Set the creation date in the format "28 Feb 2024"
    final formattedDate = '${creationDate.day} ${_monthAbbreviation(creationDate.month)} ${creationDate.year}';
    createdAt!.value = formattedDate;
  }

  String _monthAbbreviation(int month) {
    switch (month) {
      case 1:
        return 'Jan';
      case 2:
        return 'Feb';
      case 3:
        return 'Mar';
      case 4:
        return 'Apr';
      case 5:
        return 'May';
      case 6:
        return 'Jun';
      case 7:
        return 'Jul';
      case 8:
        return 'Aug';
      case 9:
        return 'Sep';
      case 10:
        return 'Oct';
      case 11:
        return 'Nov';
      case 12:
        return 'Dec';
      default:
        return '';
    }
  }
}