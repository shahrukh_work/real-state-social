import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../global/app_widgets/scroll_listener.dart';


class MainController extends GetxController{
  final ScrollController controller = ScrollController();
  late final ScrollListener model;
  var currentIndex = 0.obs;

  @override
  void onInit() {
    model = ScrollListener.initialise(controller);
    super.onInit();
  }
  void onItemTapped(int index) {
    currentIndex.value = index;
  }


  var isToggleDrawer = true.obs;
  var isNavigationToggle = true.obs;
  var tranX = 0.0.obs;
  var tranY = 0.0.obs;
  var scaleFactory = 1.0.obs;

  void drawerToggleUp() {
    isNavigationToggle.value = true;
    scaleFactory.value = 0.5;
    tranX.value = 340;
    tranY.value = 330;
    isToggleDrawer.toggle();
    isNavigationToggle.toggle();
  }

  void drawerToggleDown() {
    isNavigationToggle.value = false;
    scaleFactory.value = 1.0;
    tranX.value = 0;
    tranY.value = 0;
    isToggleDrawer.toggle();
    isNavigationToggle.toggle();
  }
}