import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:pool_real_estate/config/routes/app_routes.dart';
import 'package:pool_real_estate/constants/app_assets/app_font.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';

import '../../../global/app_theme/app_colors.dart';
import '../../../global/app_widgets/app_button.dart';
import '../controller/property_detail_and_gallary_contoller.dart';
import '../model/proper_detail_image.dart';

class PropertyDetailAndGalleryScreen extends StatefulWidget {
  PropertyDetailAndGalleryScreen({Key? key}) : super(key: key);

  @override
  _PropertyDetailAndGalleryScreenState createState() =>
      _PropertyDetailAndGalleryScreenState();
}

class _PropertyDetailAndGalleryScreenState
    extends State<PropertyDetailAndGalleryScreen> {
  final PropertyDetailAndGalleryController _propertyDetailAndGalleryController =
      Get.find<PropertyDetailAndGalleryController>();

  final PageController _pageController = PageController();

  int _currentPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController.addListener(() {
      setState(() {
        _currentPage = _pageController.page?.round() ?? 0;
      });
    });
  }

  final int maxTextLength = 8;
  var abc = "";
  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.sizeOf(context);
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    height: 260,
                    child: PageView.builder(
                      controller: _pageController,
                      onPageChanged: (value) {
                        _propertyDetailAndGalleryController
                            .updateSelectedIndex(value);
                      },
                      itemCount:
                          _propertyDetailAndGalleryController.imageList.length,
                      itemBuilder: (context, index) {
                        return ImageListWidget(
                          imageDetailModel: _propertyDetailAndGalleryController
                              .imageList[index],
                        );
                      },
                    ),
                  ),
                  Positioned(
                    top: 10,
                    left: 10,
                    child: Container(
                      child: Image.asset(AppImages.icArrowBack),
                    ),
                  ),
                  Positioned(
                    top: 10,
                    right: 20,
                    child: Container(
                      child: Image.asset(AppImages.icHeartImage),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: Container(
                  // width: 100,
                  height: 8,

                  child: Obx(() => ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: _propertyDetailAndGalleryController
                            .imageList.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              _pageController.animateToPage(index,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.ease);
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 4),
                              child: Indicators(
                                isActive: index ==
                                    _propertyDetailAndGalleryController
                                        .selectedIndex,
                                isSpecial: index == 2,
                                isCurrentPage: index == _currentPage,
                              ),
                            ),
                          );
                        },
                      )),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 12, right: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            "Apertment",
                            style: TextStyle(
                                fontFamily: AppFont.poppinsFont,
                                fontSize: 13,
                                color: AppColors.cornFlowerBlue),
                          ),
                          Text(
                            "28 Mar,2024",
                            style: TextStyle(
                              fontSize: 11,
                              fontFamily: AppFont.poppinsFont,
                              color: AppColors.veryLightGrey,
                            ),
                          ).paddingOnly(right: 16)
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Woodland Apartment",
                      style: TextStyle(
                        fontFamily: AppFont.poppinsFont,
                        color: AppColors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      "1012 Ocean avanue, New yourk, USA",
                      style: TextStyle(
                        fontFamily: AppFont.poppinsFont,
                        color: AppColors.veryLightGrey,
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 40,
                child: TabBar(
                  controller: _propertyDetailAndGalleryController.tabController,
                  tabs: [
                    Tab(text: 'Tab 1'),
                    Tab(text: 'Tab 2'),
                  ],
                ),
              ),
              // // TabBarView
              Container(
                height: 750,
                padding: EdgeInsets.only(left: 16, right: 16),
                child: TabBarView(
                  controller: _propertyDetailAndGalleryController.tabController,
                  children: [
                    // Content for Tab 1
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            height: 90,
                            child: ListView.builder(
                              itemCount: _propertyDetailAndGalleryController
                                  .descriptionList.length,
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                return Card(
                                  color: Colors.white,
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      top: 10,
                                      bottom: 13,
                                      left: 27,
                                      right: 27,
                                    ),
                                    child: GestureDetector(
                                      onTap: () {
                                        _propertyDetailAndGalleryController
                                            .changeDescriptionFruitIndex(index);
                                      },
                                      child: Obx(
                                        () {
                                          return Container(
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                SvgPicture.asset(
                                                  _propertyDetailAndGalleryController
                                                      .descriptionList[index]
                                                      .imageUrl,
                                                  color: _propertyDetailAndGalleryController
                                                              .descriptionSelectedIndex
                                                              .value ==
                                                          index
                                                      ? AppColors.pinkishMagenta
                                                      : AppColors.darkShadeBlue,
                                                ),
                                                Text(
                                                  _propertyDetailAndGalleryController
                                                      .descriptionList[index]
                                                      .title,
                                                  style: TextStyle(
                                                    color: _propertyDetailAndGalleryController
                                                                .descriptionSelectedIndex
                                                                .value ==
                                                            index
                                                        ? AppColors
                                                            .pinkishMagenta
                                                        : AppColors
                                                            .darkShadeBlue,
                                                  ),
                                                ),
                                                Text(
                                                  _propertyDetailAndGalleryController
                                                      .descriptionList[index]
                                                      .size,
                                                  style: TextStyle(
                                                    color: _propertyDetailAndGalleryController
                                                                .descriptionSelectedIndex
                                                                .value ==
                                                            index
                                                        ? AppColors
                                                            .pinkishMagenta
                                                        : AppColors
                                                            .darkShadeBlue,
                                                    fontSize: 11,
                                                    fontFamily:
                                                        AppFont.poppinsFont,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Connect with Agent ?",
                            style: TextStyle(
                              fontFamily: AppFont.poppinsFont,
                              color: AppColors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 13,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Image.asset(
                                    "assets/images/ic_person_image.png",
                                    height: 45,
                                    width: 45,
                                  ),
                                  SizedBox(width: 10),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Sandeep S.",
                                        style: TextStyle(
                                          fontFamily: AppFont.poppinsFont,
                                          color: AppColors.blueGrey,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 13,
                                        ),
                                      ),
                                      Text(
                                        "Partner",
                                        style: TextStyle(
                                          fontFamily: AppFont.poppinsFont,
                                          color: AppColors.veryLightGrey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 11,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SvgPicture.asset(
                                "assets/images/ic_message_screen.svg",
                                height: 45,
                                width: 45,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          GridView.builder(
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 4,
                                    mainAxisSpacing: 10,
                                    crossAxisSpacing: 5,
                                    mainAxisExtent: 80),
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: _propertyDetailAndGalleryController
                                .facilityModelList.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (context, index) {
                              if (_propertyDetailAndGalleryController
                                      .facilityModelList[index].title.length >
                                  maxTextLength) {
                                abc = _propertyDetailAndGalleryController
                                        .facilityModelList[index].title
                                        .substring(0, maxTextLength) +
                                    '...';
                              }
                              return Card(
                                color: Colors.white,
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 10,
                                    bottom: 13,
                                  ),
                                  child: GestureDetector(
                                    onTap: () {
                                      _propertyDetailAndGalleryController
                                          .changeFacilityIndex(index);
                                    },
                                    child: Obx(
                                      () {
                                        return Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            SvgPicture.asset(
                                              _propertyDetailAndGalleryController
                                                  .facilityModelList[index]
                                                  .imageUrl,
                                              color: _propertyDetailAndGalleryController
                                                          .facilitySelectedIndex
                                                          .value ==
                                                      index
                                                  ? AppColors.pinkishMagenta
                                                  : AppColors.darkShadeBlue,
                                            ),
                                            Text(
                                              abc,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: _propertyDetailAndGalleryController
                                                            .facilitySelectedIndex
                                                            .value ==
                                                        index
                                                    ? AppColors.pinkishMagenta
                                                    : AppColors.darkShadeBlue,
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  "Address",
                                  style: TextStyle(
                                      fontFamily: AppFont.poppinsFont,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: AppColors.darkIndigo),
                                ),
                                Text(
                                  "View on Map",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: AppFont.poppinsFont,
                                    fontWeight: FontWeight.w500,
                                    color: AppColors.veryLightGrey,
                                  ),
                                ).paddingOnly(right: 16)
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            height: 1,
                            color: AppColors.greyColor,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                SvgPicture.asset(AppImages.ic_map),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Lorem Ipsum is simply dummy text ",
                                  style: TextStyle(
                                      fontFamily: AppFont.poppinsFont,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: AppColors.darkIndigo),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 201,
                            decoration: BoxDecoration(
                                color: AppColors.appGrey.withOpacity(.5),
                                borderRadius: BorderRadius.circular(10)),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Total Price ",
                                      style: TextStyle(
                                          fontFamily: AppFont.poppinsFont,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          color: AppColors.darkIndigo),
                                    ),
                                    Text(
                                      "\$350 /month",
                                      style: TextStyle(
                                          fontFamily: AppFont.poppinsFont,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          color: AppColors.darkIndigo),
                                    ),
                                  ],
                                ),
                                AppButton(
                                    buttonWidth: mq.width * 0.4,
                                    borderRadius: 20,
                                    titleText: 'Book Now',
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    onPressed: () {
                                      Get.toNamed(actionPurposeScreen);
                                    },
                                    buttonColor: AppColors.primaryColor,
                                    textColor: AppColors.white),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    // Content for Tab 2
                    Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Obx(
                          () => Expanded(
                            child: GridView.builder(
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      mainAxisSpacing: 10,
                                      crossAxisSpacing: 5,
                                      mainAxisExtent: 150),
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _propertyDetailAndGalleryController
                                  .gallaryModelList.length,
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                return GallaryImageListWidget(
                                  gallaryModel:
                                      _propertyDetailAndGalleryController
                                          .gallaryModelList[index],
                                );
                              },
                            ),
                          ),
                          //
                          //     Expanded(
                          //   child: ListView.builder(
                          //         scrollDirection: Axis.horizontal,
                          //         physics: NeverScrollableScrollPhysics(),
                          //         shrinkWrap: true,
                          //         itemCount: _propertyDetailAndGalleryController
                          //             .gallaryModelList.length,
                          //         itemBuilder: (context, index) {
                          //           return GallaryImageListWidget(
                          //             gallaryModel:
                          //                 _propertyDetailAndGalleryController
                          //                     .gallaryModelList[index],
                          //           );
                          //         },
                          //       ),
                          // )
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                    // Content for Tab 3
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class GallaryImageListWidget extends StatelessWidget {
  GallaryImageListWidget({super.key, required this.gallaryModel});
  GallaryModel gallaryModel;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Image.asset(
          gallaryModel.imageUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class ImageListWidget extends StatelessWidget {
  final ImageDetailModel imageDetailModel;

  ImageListWidget({Key? key, required this.imageDetailModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Image.asset(
          imageDetailModel.imageUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class Indicators extends StatelessWidget {
  final bool isActive;
  final bool isSpecial;
  final bool isCurrentPage;

  const Indicators({
    Key? key,
    required this.isActive,
    required this.isSpecial,
    required this.isCurrentPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 8,
      width: 8,
      decoration: BoxDecoration(
        color: isCurrentPage ? Colors.black : Colors.grey,
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }
}
