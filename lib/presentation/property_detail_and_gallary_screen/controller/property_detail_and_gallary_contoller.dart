import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';

import '../model/proper_detail_image.dart';

class PropertyDetailAndGalleryController extends GetxController
    with SingleGetTickerProviderMixin {
  // Create a reactive variable to hold the selected index
  var selectedIndex = 0.obs;
  late TabController tabController;
  var descriptionSelectedIndex = 0.obs;
  var facilitySelectedIndex = 0.obs;
  final int maxTextLength = 5;

  @override
  void onInit() {
    tabController = TabController(length: 2, vsync: this);
    super.onInit();
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }

  final List<ImageDetailModel> imageList = [
    ImageDetailModel(imageUrl: AppImages.icDetailForDetailScreen),
    ImageDetailModel(imageUrl: AppImages.icDetailForDetailScreen),
    ImageDetailModel(imageUrl: AppImages.icDetailForDetailScreen),
    ImageDetailModel(imageUrl: AppImages.icDetailForDetailScreen)
  ].obs;
  final List<DescriptionModel> descriptionList = [
    DescriptionModel(
        imageUrl: AppImages.icDescriptionImageOne, size: "1225", title: "sqft"),
    DescriptionModel(
        imageUrl: AppImages.icbedRoomImage, size: "3.0", title: "Bedrooms"),
    DescriptionModel(
        imageUrl: AppImages.icBathRoomImage, size: "1.0", title: "BathRoom"),
    DescriptionModel(
        imageUrl: AppImages.icsaftyRank, size: "4457", title: "Safety Rank"),
  ].obs;

  final List<FacilityModel> facilityModelList = [
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Car Parking"),
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Swimming"),
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Gym & Fit"),
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Restaurant"),
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Wi-fi"),
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Pet Center"),
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Sports Cl.."),
    FacilityModel(imageUrl: AppImages.icCarImage, title: "Laundry"),
  ].obs;
  final List<GallaryModel> gallaryModelList = [
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
    GallaryModel(
      imageUrl: AppImages.ic_gallary_image,
    ),
  ].obs;
  // Function to update the selected index
  void updateSelectedIndex(int index) {
    selectedIndex.value = index;
  }

  void changeDescriptionFruitIndex(int index) {
    descriptionSelectedIndex.value = index;
  }

  void changeFacilityIndex(int index) {
    facilitySelectedIndex.value = index;
  }
}

class DescriptionModel {
  final String imageUrl;
  final String size;
  final String title;
  DescriptionModel(
      {required this.imageUrl, required this.title, required this.size});
}

class FacilityModel {
  final String imageUrl;
  final String title;
  FacilityModel({
    required this.imageUrl,
    required this.title,
  });
}

class GallaryModel {
  final String imageUrl;

  GallaryModel({required this.imageUrl});
}
