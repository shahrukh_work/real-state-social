import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';

import '../controller/property_detail_and_gallary_contoller.dart';

class PropertyDetailAndControllerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PropertyDetailAndGalleryController());
  }
}