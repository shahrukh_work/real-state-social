import 'package:get/get.dart';
import 'package:pool_real_estate/presentation/term_and_condition_screen/term_and_condition_controller.dart';


class TermAndConditionsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TermAndConditionController());
  }
}