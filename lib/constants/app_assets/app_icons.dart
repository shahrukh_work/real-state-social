class AppIcons{
  static String arrowForwardIcon = 'assets/icons/arrow_forward_icon.svg';
  static String userEmailIcon = 'assets/icons/user_email_icon.svg';
  static String userPasswordIcon = 'assets/icons/user_password_icon.svg';
  static String userNameIcon = 'assets/icons/user_name_icon.svg';
  static String passwordHiddenIcon = 'assets/icons/password_hidden_icon.svg';
  static String homeBottomBarIcon = 'assets/icons/home_bottom_bar_icon.svg';
  static String shareIcon = 'assets/icons/share_icon.svg';
  static String likeIcon = 'assets/icons/like_icon.svg';
  static String commentIcon = 'assets/icons/comment_icon.svg';
  static String notificationIcon = 'assets/icons/notification_icon.svg';
  static String menuIcon = 'assets/icons/menu_icon.svg';
  static String searchIcon = 'assets/icons/search_icon.svg';
  static String crossCloseIcon = 'assets/icons/cross_close_icon.svg';
  static String calendarIcon = 'assets/icons/calendar_icon.svg';
  static String dropDownIcon = 'assets/icons/drop_down_icon.svg';
  // static String arrowForwardIcon = 'assets/icons/arrow_forward_icon.svg';
  static String favouriteFilledIconForItems = 'assets/icons/favourite_filled_icon.svg';
  static String icProfile = 'assets/images/ic_profile_image.svg';
  static String icFavouriteImage = 'assets/images/ic_heart_icon.svg';
  static String icAboutUs = 'assets/images/ic_refactor_image.svg';
  static String icTermAndConditions = 'assets/images/ic_term_and_condition_image.svg';
  static String icLogoutImage = 'assets/images/ic_logout_image.svg';
  static String icDeleteIcon = 'assets/images/ic_huge_icon.svg';
  static String icIconForward = 'assets/images/ic_icon_forward.svg';
  static String icDropDownArrowDown = 'assets/images/errow_down.svg';
}