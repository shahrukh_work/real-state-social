import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

import 'config/routes/app_pages.dart';

late Size mq;
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Pool Real Estate',
      debugShowCheckedModeBanner: false,
      builder: (context,child){
        return MediaQuery(
          data:
          MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: child!,
        );
      },
      /// Route Management
      unknownRoute: AppPages.getUnknownRoute(),
      initialRoute: AppPages.getInitialRoute(),
      getPages: AppPages.getPages(),
    );
  }
}


