

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants/app_assets/app_font.dart';
import '../../main.dart';
import '../app_theme/app_colors.dart';

class AppTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final bool? obscureText;
  final TextInputType inputType;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final VoidCallback? onTap;
  const AppTextFormField({
    super.key,
    required this.controller,
    this.suffixIcon,
    this.obscureText,
    this.onTap,

    required this.inputType, required this.hintText, this.prefixIcon,
  });

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Container(
      height: mq.height * .06,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        // color: Colors.white.withOpacity(0.8),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 18.0,left: 10),
        child: Center(
          child: TextFormField(
            keyboardType:inputType ,
            obscureText: obscureText?? false,
            controller: controller,
            onTap: onTap,
            style:TextStyle(
              fontFamily: AppFont.poppinsFont,
                fontSize: 14,
                fontWeight: FontWeight.w100,
                color: AppColors.black
            ),


            decoration: InputDecoration(
              focusedBorder:UnderlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.black,  // Choose your desired color
                  width: 1.0,          // Choose your desired width
                ), ) ,
              prefixIcon: prefixIcon,
              hintText: hintText,
              suffixIcon: suffixIcon,
              border:  UnderlineInputBorder(
    borderSide: BorderSide(
    color: Colors.grey,  // Choose your desired color
    width: 1.0,          // Choose your desired width
    ), ),
            ),

          ),
        ),
      ),
    ) ;
  }

}