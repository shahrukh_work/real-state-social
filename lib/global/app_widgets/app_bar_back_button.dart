import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';

import '../app_theme/app_colors.dart';


class AppBarBackButton extends StatelessWidget {
  final String? appBarTitle;
  const AppBarBackButton({super.key,this.appBarTitle});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [

        Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(shape: BoxShape.circle,color: AppColors.white),
          child: InkWell(
              onTap: ()=>Get.back(),
              child: Icon(Icons.arrow_back, size: 19,color: AppColors.black,)),
        ),
        const SizedBox(width: 20,),
        Text(appBarTitle??'',style: TextStyle(fontSize: 18,color: AppColors.black),),
      ],
    );
  }
}