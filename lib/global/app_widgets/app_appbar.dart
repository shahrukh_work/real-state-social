import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:pool_real_estate/constants/app_assets/app_images.dart';

import '../../constants/app_assets/app_icons.dart';
import '../../main.dart';
import '../../presentation/main/controllers/main_controller.dart';
import '../app_theme/app_colors.dart';

class AppAppBar extends StatelessWidget {
  AppAppBar( {Key? key, this.title, required this.openDrawer,required this.isDrawerOpen,}) : super(key: key);
  final MainController _mainLandingController = Get.put(MainController());
  final VoidCallback openDrawer;
  final String? title;
  bool isDrawerOpen;

  @override
  Widget build(BuildContext context) {
    mq = MediaQuery.sizeOf(context);
    return Container(
      padding: const EdgeInsets.only(left: 10, top: 35, bottom: 0),
      decoration: BoxDecoration(
        color:isDrawerOpen
            ? Colors.black.withOpacity(.1)
            : AppColors.appBgColor,
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          // crossAxisAlignment: CrossAxisAlignment.start,

          children: [
            InkWell(
              onTap: () => openDrawer(),
              child: SvgPicture.asset(
                AppIcons.menuIcon,
                height: 15,
                width: 15,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                title == 'property'
                    ? SvgPicture.asset(
                        AppIcons.searchIcon,
                        height: 20,
                        width: 20,
                      )
                    : Container(),
                SizedBox(
                  width: mq.width * 0.05,
                ),
                SvgPicture.asset(
                  AppIcons.notificationIcon,
                  height: 20,
                  width: 20,
                ),
                SizedBox(
                  width: mq.width * 0.05,
                ),
                Image.asset(
                  AppImages.profilePic,
                  width: 35,
                  fit: BoxFit.fill,
                  height: 35,
                ),
                SizedBox(
                  width: mq.width * 0.03,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
