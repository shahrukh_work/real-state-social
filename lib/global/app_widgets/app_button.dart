
import 'package:flutter/material.dart';

import '../../main.dart';
import '../app_theme/app_colors.dart';
import 'app_text_widget.dart';

class AppButton extends StatelessWidget {
  final String titleText;
  final VoidCallback? onPressed;
  final Color? textColor;
  final Color? buttonColor;
  final Color? borderColor;
  final double? fontSize;
  final double? borderRadius;
  final FontWeight? fontWeight;
  final double? buttonWidth;
  const AppButton({
    super.key,
    required this.titleText,
    required this.onPressed,
     this.buttonColor,
     this.textColor,
    this.borderColor, this.fontSize, this.fontWeight, this.borderRadius, this.buttonWidth,
  });

  @override
  Widget build(BuildContext context) {
    mq= MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: mq.height * .055,
        width: buttonWidth ?? mq.width * 1,
        decoration: BoxDecoration(
          color: buttonColor ?? AppColors.primaryColor,
          borderRadius: BorderRadius.circular(borderRadius ?? 10),
          border: Border.all(
            color: borderColor ??AppColors.primaryColor,
          ),
        ),
        child: Center(
            child: AppTextWidget(
              text: titleText,
              fontSize: fontSize ?? 17,
              fontWeight:fontWeight?? FontWeight.w600,
              color: textColor ?? AppColors.white,
            )
        ),
      ),
    );
  }
}