
import 'package:flutter/material.dart';

class AppColors{
  static const Color black = Colors.black;
  static const Color white = Colors.white;
  static  Color lightWhite = Color(0x93FFFFFF);
  static  Color appBgColor = Color(0xFFF3F3F3);
  static const Color transparent = Colors.transparent;
  // static const Color yellow = Colors.amber;
  static const Color yellow = Color(0xFFFFA500);
  static const Color blue = Colors.blue;
  static  Color primaryColor = Color(0xFF4A4DF0);
  static  Color grey =  Colors.grey.withOpacity(0.6);
  static  Color appGrey =  Color(0xFF636363);
  static  Color greyColor =  Color(0xFFD9D9D9).withOpacity(0.87);
  static  Color greenColor = Color(0xFF00A814);
  static  Color lightGreen = Color(0xFFAFD98F);
  static  Color cornFlowerBlue = Color(0xff478FF1);
  static  Color veryLightGrey = Color(0xff8C8C8C);
  static  Color  darkShadeBlue = Color(0xff153050);
  static  Color  pinkishMagenta = Color(0xff006EFF);
  static  Color  blueGrey = Color(0xff2A2B3F);
  static  Color   darkIndigo  = Color(0xff2A2B3F);
}