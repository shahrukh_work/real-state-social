import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:pool_real_estate/presentation/auth/forgot_password/views/create_new_password_screen.dart';
import 'package:pool_real_estate/presentation/auth/forgot_password/views/forgot_password_screen.dart';
import 'package:pool_real_estate/presentation/auth/login/views/login_screen.dart';
import 'package:pool_real_estate/presentation/auth/signup/views/signup_screen.dart';
import 'package:pool_real_estate/presentation/auth/verify_account/views/verify_account_screen.dart';
import 'package:pool_real_estate/presentation/main/views/main_screen.dart';
import 'package:pool_real_estate/presentation/splash/views/splash_screen.dart';
import 'package:pool_real_estate/presentation/walk_through/views/walk_through_screen_one.dart';
import 'package:pool_real_estate/presentation/walk_through/views/walk_through_screen_three.dart';
import 'package:pool_real_estate/presentation/walk_through/views/walk_through_screen_two.dart';

import '../../presentation/about_us_screen/about_us_binding.dart';
import '../../presentation/about_us_screen/about_us_screen.dart';
import '../../presentation/action_purpose_screen/action_purpose_binding.dart';
import '../../presentation/action_purpose_screen/action_purpose_screen.dart';
import '../../presentation/fav_screen/favorite_binding.dart';
import '../../presentation/fav_screen/favorite_screen.dart';
import '../../presentation/profile/profile_binding.dart';
import '../../presentation/profile/profile_screen.dart';
import '../../presentation/property_detail_and_gallary_screen/property_detail_and_gallary_binding/property_detail_and_controller_binding.dart';
import '../../presentation/property_detail_and_gallary_screen/view/property_detail_and_gallary_screen.dart';
import '../../presentation/term_and_condition_screen/term_and_condition_binding.dart';
import '../../presentation/term_and_condition_screen/term_and_screen.dart';
import '../../presentation/unknown_route/unknown_route_screen.dart';
import 'app_routes.dart';

const String splashPage = '/splashPage';

class AppPages {
  static getUnknownRoute() {
    return GetPage(
      name: noPageFound,
      page: () => const UnknownRouteScreen(),
      transition: Transition.zoom,
    );
  }

  static getInitialRoute() {
    // return splashScreen;
    // return otpScreen;
    return mainScreen;
    // return paymentMethodScreen;
  }

  static getPages() {
    return [
      GetPage(
        name: splashScreen,
        page: () =>  SplashScreen(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: walkThroughScreenOne,
        page: () =>  WalkThroughScreenOne(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: walkThroughScreenTwo,
        page: () =>  WalkThroughScreenTwo(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: walkThroughScreenThree,
        page: () =>  WalkThroughScreenThree(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: loginScreen,
        page: () =>  LoginScreen(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: signupScreen,
        page: () =>  SignupScreen(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: verifyAccountScreen,
        page: () =>  VerifyAccountscreen(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: createNewPasswordScreen,
        page: () =>  CreateNewPasswordScreen(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: forgotPasswordScreen,
        page: () =>  ForgotPasswordScreen(),
        // binding: LandingBinding(),
      ),
      GetPage(
        name: imageDetailAndGalleryScreen,
        page: () =>  PropertyDetailAndGalleryScreen(),
        binding: PropertyDetailAndControllerBinding(),
      ),
      GetPage(
        name: profileScreen,
        page: () =>  ProfileScreen(),
        binding: ProfileBinding(),
      ),
      GetPage(
        name: favoriteScreen,
        page: () =>  FavrioteScreen(),
        binding: FavoriteBinding(),
      ),
      GetPage(
        name: aboutUsScreenDrawer,
        page: () =>  AboutUsScreen(),
        binding: AboutUsBinding(),
      ),   GetPage(
        name: actionPurposeScreen,
        page: () =>  ActionPurposeScreen(),
        binding: ActionPurposeBinding(),
      ),
      GetPage(
        name: termAndConditionScreen,
        page: () =>  TermAndConditionsScreen(),
        binding: TermAndConditionsBinding(),
      ),
      GetPage(
        name: mainScreen,
        page: () =>  MainScreen(),
        // binding: LandingBinding(),
      ),



    ];
  }

// //// get map
//   static to(String route, {Map<String, dynamic>? arguments}) =>
//       Get.toNamed(route, arguments: arguments);
//
//   static offAllTo(String route, {Map<String, dynamic>? arguments}) =>
//       Get.offAllNamed(route, arguments: arguments);
//
//   static offTo(String route, {Map<String, dynamic>? arguments}) =>
//       Get.offNamed(route, arguments: arguments);
//
//   static back() => Get.back();
}
